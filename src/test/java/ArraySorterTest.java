import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import junit.framework.TestCase;

@RunWith(Parameterized.class)
public class ArraySorterTest extends TestCase {

    private String input;
    private String output;

    public ArraySorterTest(String input, String output) {
        this.input = input;
        this.output = output;
    }

    @Test
    public void testGetSortedArray() {
        System.setIn(new ByteArrayInputStream(input.getBytes()));
        assertEquals(output, ArraySorter.getSortedArrayFromCommandLine());
    }

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                { "1 2 3 4 5 6 7 8 9 1 0", "[1, 1, 2, 3, 4, 5, 6, 7, 8, 9]" },
                { "1 2 3 4 5 6 7 8 9 10 11", "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]" },
                { "1 2 3 4 5 6 7 8 9 10 0", "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]" },
                { "1 2 3 8 6 5 7 4 9 2 0", "[1, 2, 2, 3, 4, 5, 6, 7, 8, 9]" },
                { "1 2 3 4 5 6 7 8 9 0", "[1, 2, 3, 4, 5, 6, 7, 8, 9]" },
                { "1 0", "[1]" },
                { "0", "Empty array" }
        });
    }
}