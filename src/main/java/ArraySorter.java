import java.util.Arrays;
import java.util.Scanner;

public class ArraySorter {

    /**
     * Asks user to input integers from command line
     * @return sorted array stringified
     *
     * Inputting 0 ends array and any integer after 0 won't be considered as part of output array
     */
    public static String getSortedArrayFromCommandLine() {
        System.out.println("Please provide up to ten integers. Input 0 to finish array: ");
        Scanner scanner = new Scanner(System.in);
        int[] inputArray = new int[10];
        int count = 0;
        while (scanner.hasNext() && count <= 10) {
            int next = scanner.nextInt();
            if (next == 0) {
                System.out.println("End of array. Sorting integers and printing");
                break;
            }
            if (count == 10) {
                System.out.println("More than 10 integers. Sorting first ten of them and printing");
                break;
            }
            inputArray[count] = next;
            count++;
        }
        if (count == 0) {
            return "Empty array";
        }
        inputArray = Arrays.copyOf(inputArray, count);
        sortArray(inputArray);
        return Arrays.toString(inputArray);
    }

    private static void sortArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }
}
